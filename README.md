# spotify-playlist

Python project for Spotify API interaction.

## Name
my-playlist

## Description
This is a Python project which uses Spotify API.
Source https://github.com/JasonLeviGoodison/SpotifyToYoutubeMP3

The goal is to test OAuth2, learn more about Flask and use CI for linting. Flake8 is installed locally to test linting locally on Fedora 36.

## pre-requisites
1. install python 3.7.x for your system
https://docs.python.org/3/using/index.html

check python version with `python --version`

2. install pip 
https://packaging.python.org/en/latest/guides/installing-using-linux-tools/
check pip version with `pip --version`

3. You need to have Spotify's account to access https://developer.spotify.com/



## Installation
1. Clone or download this repository
2. `cd` into the cloned/downloaded directory
3. Create `.env` file in a working directory.
4. Save `client_id=your-client-id-from-spotify and client_secret=your-client-secret-from-spotify` in `.env` file. Find more details [here](https://spotipy.readthedocs.io/en/master/#installation)
5. Run the script `install-modules.sh` or install all modules used.
6. Run script `run-flask-app.sh` to start app on localhost. More info on flask [website](https://flask.palletsprojects.com/en/2.2.x/installation/)


## Usage
Once the the user is authorized, I decided to print the list of songs instead of saving the CSV file.


## test with Selenium & pre-installed Firefox on Fedora 36
1. Download the gecko driver for Fedora 36
https://github.com/mozilla/geckodriver/releases/download/v0.31.0/geckodriver-v0.31.0-linux64.tar.gz

2. Extract and move the downloaded driver to `/usr/local/bin`
`cd ~/Downloads/geckodriver-v0.31.0-linux64`
`sudo mv geckodriver /usr/local/bin/geckodriver`

3. Add geckodrive to path
`sudo vim ~/.bashrc`
add this line at the end of the file `export PATH=$PATH:/usr/local/bin/geckodriver` and save and exit

4. Run `source ~/.bashrc`

5. Install python [bindings](https://selenium-python.readthedocs.io/installation.html)
`pip install selenium`
