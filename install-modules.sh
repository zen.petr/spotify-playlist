# install spotipy
echo "Installing Spotipy"
pip install spotipy --upgrade

# install requests
echo "installing requests"
python -m pip install requests

# install lxml
echo "installing lxml"
python -m pip install lxml

# install flask
echo "Installing flask"
python -m pip install Flask

# install pandas
echo "Installing pandas"
python -m pip install pandas

# install dotenv
echo "Installing dotenv"
python -m pip install python-dotenv