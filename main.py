import spotipy
from spotipy.oauth2 import SpotifyOAuth
import os
import time
import requests
import re
from lxml import html
from flask import Flask, url_for, redirect, session, request, jsonify
import pandas as pd
from dotenv import load_dotenv
load_dotenv()

client_id = os.environ.get('CLIENT_ID')
client_secret = os.environ.get('CLIENT_SECRET')


app = Flask(__name__)

app.secret_key = 'SOMETHING-RANDOM'
app.config['SESSION_COOKIE_NAME'] = 'spotify-login-session'


@app.route("/")
def login():
    sp_oauth = create_spotify_oauth()
    auth_url = sp_oauth.get_authorize_url()
    return redirect(auth_url)


@app.route('/authorize')
def authorize():
    sp_oauth = create_spotify_oauth()
    session.clear()
    code = request.args.get('code')
    token_info = sp_oauth.get_access_token(code)
    session["token_info"] = token_info
    return redirect("/getTracks")


@app.route('/logout')
def logout():
    for key in list(session.keys()):
        session.pop(key)

    return redirect('/')


@app.route('/getTracks')
def get_all_tracks():
    sp = get_auth_header()
    results = []
    iter = 0
    while True:
        offset = iter * 50
        iter += 1
        curGrp = sp.current_user_saved_tracks(limit=50, offset=offset)['items']
        for idx, item in enumerate(curGrp):
            track = item['track']
            val = track['name'] + " - " + track['artists'][0]['name']
            results += [val]
        if (len(curGrp) < 50):
            break

    df = pd.DataFrame(results, columns=["song names"])
    print(df)
    # df.to_csv('songs.csv', index=False)
    return "done"


@app.route('/getPlaylist')
def getPlaylist():
    sp = get_auth_header()
    playlist = sp.playlist('1gemzxVimHjRfKpeN7CDnV')
    return jsonify(playlist)

@app.route('/getSongs', methods=['GET', 'POST'])
def getSongs():
    session = requests.Session()
    session.cookies.clear()
    session.head('https://www.radio1.cz/wp-admin/admin-ajax.php')
    headers =  {
        "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:95.0) Gecko/20100101 Firefox/95.0",
        "Accept": "*/*",
        "Accept-Language": "en-US,en;q=0.5",
        "Content-Type": "application/x-www-form-urlencoded",
        "Sec-Fetch-Dest": "empty",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Site": "same-origin"
    }
    '''
    data = {
        "referrer": 'https://www.radio1.cz/porad/shadowbox/?action=morebroadcast&program=1609%2C1756%2C3623%2C4775%2C5817%2C7323%2C8908%2C10297%2C11807%2C12923%2C14165%2C15313%2C16814%2C18161%2C20019%2C21588%2C22794%2C25012%2C26133%2C27475',
        "body": "action=morebroadcast&page=3&program=1609%2C1756%2C3623%2C4775%2C5817%2C7323%2C8908%2C10297%2C11807%2C12923%2C14165%2C15313%2C16814%2C18161%2C20019%2C21588%2C22794%2C25012%2C26133%2C27475%2C28288"
        #"method": "POST",
        #"mode": "cors"
        }
        '''

    response = session.post(url='https://www.radio1.cz/porad/shadowbox/?action=morebroadcast&program=1609%2C1756%2C3623%2C4775%2C5817%2C7323%2C8908%2C10297%2C11807%2C12923%2C14165%2C15313%2C16814%2C18161%2C20019%2C21588%2C22794%2C25012%2C26133%2C27475%2C28288',
        headers=headers,
        
        #data=data
        
            )
    string_data = response.content.decode()
    html_data = re.sub(r'\(|\)', '', string_data)
    dom_tree = html.fromstring(html_data)
    li_tag = dom_tree.xpath('//html/body/main/section[3]/div[2]/div/div[1]/div/ul/text()')
    
    
    print(session.cookies)
    print(type(dom_tree))
    print('li tags', li_tag)
        
    return 'songs'

# next step get track id
@app.route('/search')
def get_more_info():
    sp = get_auth_header()
    # name = 'Eminem'
    track_name = 'never stop dancing'
    tracks_result = sp.search(q='track:' + track_name, type='track')
    # artist name
    print(tracks_result['tracks']['items'][0]['artists'][0]['name'])
    # track name
    print(tracks_result['tracks']['items'][0]['name'])
    # track id
    print(tracks_result['tracks']['items'][0]['id'])
    track_uri = tracks_result['tracks']['items'][0]['uri']
    return track_uri


# get track features
@app.route('/push')
def push_to_playlist():
    sp = get_auth_header()
    track_id = get_more_info()
    print(track_id)
    # creates duplicates
    sp.playlist_add_items(
        playlist_id='1gemzxVimHjRfKpeN7CDnV', items=[track_id], position=None)
    return track_id


# get headers
def get_auth_header():
    session['token_info'], authorized = get_token()
    session.modified = True
    if not authorized:
        return redirect('/')
    sp = spotipy.Spotify(auth=session.get('token_info').get('access_token'))
    return sp


# Checks to see if token is valid and gets a new token if not
def get_token():
    token_valid = False
    token_info = session.get("token_info", {})

    # Checking if the session already has a token stored
    if not (session.get('token_info', False)):
        token_valid = False
        return token_info, token_valid

    # Checking if token has expired
    now = int(time.time())
    is_token_expired = session.get('token_info').get('expires_at') - now < 60

    # Refreshing token if it has expired
    if (is_token_expired):
        sp_oauth = create_spotify_oauth()
        token_info = sp_oauth.refresh_access_token
        (session.get('token_info').get('refresh_token'))

    token_valid = True
    return token_info, token_valid


def create_spotify_oauth():
    return SpotifyOAuth(
        client_id=client_id,
        client_secret=client_secret,
        redirect_uri=url_for('authorize', _external=True),
        scope="user-library-read"
    )
