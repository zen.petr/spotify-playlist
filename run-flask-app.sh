#!/bin/bash -e

export FLASK_APP=main.py
export FLASK_ENV=development
python -m flask run --host=localhost --port=3030