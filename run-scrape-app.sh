#!/bin/bash -e

export FLASK_APP=driver.py
export FLASK_ENV=development
python -m flask run --host=localhost --port=3030