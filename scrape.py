#!/usr/bin/env python
import requests
from bs4 import BeautifulSoup
'''
shows
example urls
https://www.radio1.cz/porad/estereo/
https://www.radio1.cz/porad/shadowbox/
https://www.radio1.cz/porad/shadowbox/
'''
shows = ['shadowbox', 'cdnonstop', 'estereo']
#print(shows[0])

def check_user_input(input):
    try:
        value = int(input)
        if value in range(len(shows)+1):
            print('this is the', value)
        else:
            print('show not listed')
    except ValueError:
        print('Please enter a number from this list')

def ask_for_show():
    print('1: Shadowbox')
    print('2: Cd Nonstop')
    print('3: Estero')
    
    show = input('please select your favorite show ')
    return check_user_input(show)

ask_for_show()


def get_response(url):
    r = requests.get(url)
    if not r.ok:
        raise Exception(f'Http request failed with status {r.status_code}')
    return r

# function to change the url
#response = BeautifulSoup(get_response('https://www.radio1.cz/porad/shadowbox/').text, 'html.parser')

def get_li_elements(html_doc):
    li_elements = html_doc.select('#tracklist .mt1 li')
    return li_elements

def is_trackname(li_element):
    li_classes = li_element.get('class', [])
    return 'mb1' not in li_classes


tracklist = [li.text for li in get_li_elements(response) if is_trackname(li)]

print(tracklist)



