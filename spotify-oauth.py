import spotipy
from spotipy.oauth2 import SpotifyOAuth
import os
import time
import requests
import re
from lxml import html
from flask import Flask, url_for, redirect, session, request, jsonify
import pandas as pd
from dotenv import load_dotenv
load_dotenv()

client_id = os.environ.get('CLIENT_ID')
client_secret = os.environ.get('CLIENT_SECRET')


app = Flask(__name__)

app.secret_key = 'SOMETHING-RANDOM'
app.config['SESSION_COOKIE_NAME'] = 'spotify-login-session'


@app.route("/")
def login():
    sp_oauth = create_spotify_oauth()
    auth_url = sp_oauth.get_authorize_url()
    return redirect(auth_url)


@app.route('/authorize')
def authorize():
    sp_oauth = create_spotify_oauth()
    session.clear()
    code = request.args.get('code')
    token_info = sp_oauth.get_access_token(code)
    session["token_info"] = token_info
    return redirect("/getTracks")

# Get a list of the songs saved in the current Spotify user's 'Your Music' library.
@app.route('/getTracks')
def get_all_tracks():
    sp = get_auth_header()
    results = []
    iter = 0
    while True:
        offset = iter * 50
        iter += 1
        curGrp = sp.current_user_saved_tracks(limit=50, offset=offset)['items']
        for idx, item in enumerate(curGrp):
            track = item['track']
            val = track['name'] + " - " + track['artists'][0]['name']
            results += [val]
        if (len(curGrp) < 50):
            break

    df = pd.DataFrame(results, columns=["song names"])
    print(df)
    # df.to_csv('songs.csv', index=False)
    return "done"


@app.route('/getSongs', methods=['GET', 'POST'])
def getSongs():
    session = requests.Session()
    session.cookies.clear()
    session.head('https://www.radio1.cz/wp-admin/admin-ajax.php')
       
    return 'songs'


# get headers
def get_auth_header():
    session['token_info'], authorized = get_token()
    session.modified = True
    if not authorized:
        return redirect('/')
    sp = spotipy.Spotify(auth=session.get('token_info').get('access_token'))
    return sp


# Checks to see if token is valid and gets a new token if not
def get_token():
    token_valid = False
    token_info = session.get("token_info", {})

    # Checking if the session already has a token stored
    if not (session.get('token_info', False)):
        token_valid = False
        return token_info, token_valid

    # Checking if token has expired
    now = int(time.time())
    is_token_expired = session.get('token_info').get('expires_at') - now < 60

    # Refreshing token if it has expired
    if (is_token_expired):
        sp_oauth = create_spotify_oauth()
        token_info = sp_oauth.refresh_access_token
        (session.get('token_info').get('refresh_token'))

    token_valid = True
    return token_info, token_valid


def create_spotify_oauth():
    return SpotifyOAuth(
        client_id=client_id,
        client_secret=client_secret,
        redirect_uri=url_for('authorize', _external=True),
        scope="user-library-read"
    )
