# temp test file
# headers
'''
headers =  {
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:104.0) Gecko/20100101 Firefox/104.0",
    "Accept": "*/*",
    "Accept-Language": "en-US,en;q=0.5",
    "Content-Type": "application/x-www-form-urlencoded",
    "Sec-Fetch-Dest": "empty",
    "Sec-Fetch-Mode": "cors",
    "Sec-Fetch-Site": "same-origin",
    "Accept-Encoding": "gzip, deflate, br",
    "Connection": "keep-alive",
    "Content-Length": "338",
    "Host": "www.radio1.cz",
    "Origin": "https://www.radio1.cz",
    "Referer": "https://www.radio1.cz/porad/shadowbox/",
    "TE": "trailers",
    "DNT": "1"

}
'''

'''
button with id=morebroadcast
data-page=1
data-program=1234,1567, etc.
'''


from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from time import sleep
import urllib.request
import urllib.parse
import pandas as pd

# create the initial window
driver = webdriver.Firefox()
  
# go to the home page test
driver.get('https://www.radio1.cz/porad/shadowbox/')
sleep(5)  
# storing the current window handle to get back to dashboard
main_page = driver.current_window_handle
  
# wait for page to load completely
WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='didomi-notice-agree-button']"))).click();
driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='morebroadcast']"))).click();

el = driver.find_element(By.XPATH, "//*[@id='morebroadcast']")


program = el.get_attribute("data-program")


def save_playlist():
    

    df = pd.Series([program])
    #print(df)
    df.to_csv('songs.csv', index=False)
    return "done"

save_playlist()

    
def open_read_data():
    with open('songs.csv') as customers:
        c =customers.read()
        print(c)
        print(type(c))
    return c

open_read_data()


